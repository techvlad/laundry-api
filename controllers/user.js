const moment = require('moment');

const db = require('../models/index');
const { removeTableNameFromAttributes } = require('../utils');

const UserModel = db.user;
const WasherModel = db.washer;
const QueueModel = db.queue;


const DATE_KEY_FORMAT = 'YYYY-MM-DD';
const TIME_KEY_FORMAT = 'HH:mm';

const FETCH_DAY_COUNT = 3;

class UserController {
  static async Login(ctx) {
    const { login, password } = ctx.request.body;

    if (!login || !password) {
      ctx.throw(400, 'Логин или пароль не задан');
    }

    const user = await UserModel.findOne({ where: { email: login }, raw: true });

    if (!user) {
      ctx.throw(400, 'Пользователь не существует');
    }

    if (user.password !== password) {
      ctx.throw(400, 'Не правильный логин или пароль');
    }

    delete user.password;

    ctx.session = { user };

    ctx.body = { user };
    ctx.status = 200;
  }

  static async Logout(ctx) {
    ctx.session = null;
    ctx.status = 200;
  }

  static async Reserve(ctx) {
    const { date, time, washer } = ctx.request.body;

    const washersPool = ctx.session.user.room < 500 ? [1, 2, 3, 4] : [5];

    if (washersPool.indexOf(Number(washer)) === -1) {
      ctx.throw(400, 'Стиральная машина не доступна');
    }

    if (!date || !time || !washer) {
      ctx.throw(400, 'Bad arguments');
    }

    // Check date
    const targetDate = moment(`${date} ${time}`);
    const currentDate = moment();

    if (currentDate > targetDate) {
      ctx.throw(400, 'Время вышло');
    }

    const user = await UserModel.findOne({ where: { id: ctx.session.user.id } });

    if (user.dataValues.points < 1) {
      ctx.throw(400, 'Не достаточно бонусов');
    }

    const currentReservation = await QueueModel.find({
      where: { date: targetDate, washerID: washer },
      raw: true,
    });

    if (currentReservation) {
      ctx.throw(400, 'Уже зарезервировано');
    }

    user.points -= 1;
    await user.save();

    const newReserve = QueueModel.build({
      date: targetDate,
      userID: ctx.session.user.id,
      washerID: washer,
    });

    await newReserve.save();

    ctx.status = 200;
  }

  static async Schedule(ctx) {
    // Fetch information about next FETCH_DAY_COUNT days
    const start = moment().startOf('day');
    const end = moment().startOf('day').add({ days: FETCH_DAY_COUNT });

    // Create template object for response
    const response = {};

    const washersPool = ctx.session.user.room < 500 ? [1, 2, 3, 4] : [5];

    // Days
    for (let date = moment(start); date.diff(end, 'd') < 0; date.add(1, 'd')) {
      // Hours
      for (date.hour(6); date.hour() < 23; date.add(1, 'h')) {
        if (date.hour() > 10 && date.hour() < 16) {
          date.hour(16);
        }

        const dateKey = date.format(DATE_KEY_FORMAT);
        const timeKey = date.format(TIME_KEY_FORMAT);

        if (!response[dateKey]) {
          response[dateKey] = {};
        }

        if (!response[dateKey][timeKey]) {
          response[dateKey][timeKey] = {};
        }

        response[dateKey][timeKey] = washersPool
          .map(e => ({ [e]: false }))
          .reduce((acc, cur) => ({ ...acc, ...cur }), {});
      }
    }

    const schedule = removeTableNameFromAttributes(await QueueModel.findAll({
      where: {
        date: {
          $between: [start.toDate(), end.toDate()],
        },
        washerID: washersPool,
      },
      attributes: ['date'],
      include: [
        { model: UserModel, attributes: [['fullName', 'name'], 'room'] },
        { model: WasherModel, attributes: [['title', 'washer']] },
      ],
      raw: true,
    }));

    // Populating with a template object
    schedule.forEach((e) => {
      const dateKey = moment(e.date).format(DATE_KEY_FORMAT);
      const timeKey = moment(e.date).format(TIME_KEY_FORMAT);

      response[dateKey][timeKey][e.washer] = e;
    });

    ctx.body = { schedule: response };
  }
}

module.exports = UserController;
