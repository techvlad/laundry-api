/**
 * Remove fields table name from attributes names
 * e.g. { 'table.attribute': 'value' } convert into { 'attribute': 'value' }
 */

module.exports.removeTableNameFromAttributes = (queryResult) => {
  const result = [];

  queryResult.forEach((obj) => {
    const tempObject = {};

    Object.entries(obj).forEach(([key, value]) => {
      const keyPrefixes = key.split('.');
      const newKey = keyPrefixes.length > 1
        ? keyPrefixes[keyPrefixes.length - 1]
        : keyPrefixes[0];

      tempObject[newKey] = value;
    });

    result.push(tempObject);
  });

  return result;
};
