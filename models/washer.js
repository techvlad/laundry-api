'use strict';
module.exports = (sequelize, DataTypes) => {
  var washer = sequelize.define('washer', {
    title: DataTypes.STRING,
    isWorking: DataTypes.BOOLEAN,
  }, {});
  washer.associate = function(models) {
    // associations can be defined here
  };
  return washer;
};
