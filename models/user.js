'use strict';
module.exports = (sequelize, DataTypes) => {
  var user = sequelize.define('user', {
    fullName: DataTypes.STRING,
    email: DataTypes.STRING,
    password: DataTypes.STRING,
    room: DataTypes.INTEGER,
    points: DataTypes.INTEGER,
  }, {});
  user.associate = function(models) {
    // associations can be defined here
  };
  return user;
};
