// Load env settings
require('dotenv-safe').config();

const { resolve } = require('path');

const Koa = require('koa');
const KoaHelmet = require('koa-helmet');
const KoaLogger = require('koa-logger');
const KoaSession = require('koa-session');
const KoaMount = require('koa-mount');
const KoaStatic = require('koa-static');
const KoaBody = require('koa-body');
const KoaCors = require('@koa/cors');

const {
  PORT,
  NODE_ENV,
  USER_STATIC,
  SECRET_KEY,
} = process.env;

const app = new Koa();
const router = require('./router');

if (NODE_ENV === 'development') {
  app.use(KoaLogger());
}

// Add body parser
app.use(KoaBody({ multipart: true }));

// Add important security headers
app.use(KoaHelmet());

// Setup sessions
app.keys = [SECRET_KEY];

const sessionConfig = {
  key: 'session',
  maxAge: 86400000,
  overwrite: true,
  httpOnly: true,
  signed: true,
  rolling: true,
  renew: true,
};

app.use(KoaSession(sessionConfig, app));

// Serve static files
app.use(KoaMount('/', KoaStatic(resolve(__dirname, USER_STATIC))));


// API router
app.use(KoaMount('/api', router.routes()));
// app.use(KoaMount('/api', router.allowedMethods()))

// CORS
app.use(KoaCors());

// Start server
app.listen(PORT, () => console.log(`Server started at ${PORT} port`));
