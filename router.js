const KoaRouter = require('koa-router');

const SessionGuard = require('./middlewares/SessionGuard');

const UserController = require('./controllers/user');
const AdminController = require('./controllers/admin');

const router = new KoaRouter();

// Main router
router.get('/', (ctx) => {
  ctx.body = { message: 'API is working' };
});

// User router
const userRouter = new KoaRouter();
userRouter.post('/login', UserController.Login);
userRouter.post('/logout', UserController.Logout);
userRouter.post('/reserve', SessionGuard, UserController.Reserve);
userRouter.get('/schedule', UserController.Schedule);

// Admin router
const adminRouter = new KoaRouter();
adminRouter.post('/login', AdminController.Login);
adminRouter.get('/log', AdminController.fetchLog);
adminRouter.get('/users', AdminController.fetchUsers);
adminRouter.put('/users', AdminController.setUserPoints);
adminRouter.post('/import', AdminController.getFileFromUser);

// Unite routes
router.use('/user', userRouter.routes(), userRouter.allowedMethods());
router.use('/admin', adminRouter.routes(), userRouter.allowedMethods());

module.exports = router;
