module.exports = async (ctx, next) => {
  if (!ctx.session.user) {
    ctx.throw(401, 'Доступ запрещен');
  }

  await next();
};
