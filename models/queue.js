'use strict';
module.exports = (sequelize, DataTypes) => {
  var queue = sequelize.define('queue', {
    date: DataTypes.DATE,
    userID: DataTypes.INTEGER,
    washerID: DataTypes.INTEGER,
  }, {})
  queue.associate = function(models) {
    // associations can be defined here
    queue.belongsTo(models.user, { foreignKey: 'userID', name: 'user'});
    queue.belongsTo(models.washer, { foreignKey: 'washerID', name: 'washer'});
  };
  return queue;
};
