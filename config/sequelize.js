require('dotenv-safe').config();

const {
  DB_USER,
  DB_PASSWORD,
  DB_DATABASE,
  DB_HOST,
  DB_DIALECT,
} = process.env;

const config = {
  username: DB_USER,
  password: DB_PASSWORD,
  database: DB_DATABASE,
  host: DB_HOST,
  dialect: DB_DIALECT,
};

module.exports = {
  development: { ...config },
  test: { ...config },
  production: { ...config },
};
